import org.jbox2d.callbacks.ContactImpulse
import org.jbox2d.callbacks.ContactListener
import org.jbox2d.collision.Manifold
import org.jbox2d.dynamics.contacts.Contact

class IContactListener(callback: IContactListener.() -> Unit) : ContactListener {
    var onBeginContact: ((contact: Contact) -> Unit)? = null
    var onEndContact: ((contact: Contact) -> Unit)? = null
    var beforeSolve: ((contact: Contact, oldManifold: Manifold) -> Unit)? = null
    var afterSolve: ((contact: Contact, impulse: ContactImpulse) -> Unit)? = null

    companion object {
        val default = IContactListener {
            onBeginContact = { contact ->
                (contact.getFixtureA()?.userData as? DynamicSpriteData)?.onBeginContact?.invoke(contact.getFixtureA(), contact.getFixtureB(), contact)
                (contact.getFixtureB()?.userData as? DynamicSpriteData)?.onBeginContact?.invoke(contact.getFixtureB(), contact.getFixtureA(), contact)
            }
            onEndContact = { contact ->
                (contact.getFixtureA()?.userData as? DynamicSpriteData)?.onEndContact?.invoke(contact.getFixtureA(), contact.getFixtureB(), contact)
                (contact.getFixtureB()?.userData as? DynamicSpriteData)?.onEndContact?.invoke(contact.getFixtureB(), contact.getFixtureA(), contact)
            }
            beforeSolve = { contact, oldManifold ->
                (contact.getFixtureA()?.userData as? DynamicSpriteData)?.beforeSolve?.invoke(contact.getFixtureA(), contact.getFixtureB(), contact, oldManifold)
                (contact.getFixtureB()?.userData as? DynamicSpriteData)?.beforeSolve?.invoke(contact.getFixtureB(), contact.getFixtureA(), contact, oldManifold)
            }
            afterSolve = { contact, impulse ->
                (contact.getFixtureA()?.userData as? DynamicSpriteData)?.afterSolve?.invoke(contact.getFixtureA(), contact.getFixtureB(), contact, impulse)
                (contact.getFixtureB()?.userData as? DynamicSpriteData)?.afterSolve?.invoke(contact.getFixtureB(), contact.getFixtureA(), contact, impulse)
            }
        }
    }

    init {
        this.apply(callback)
    }

    override fun beginContact(contact: Contact) {
        onBeginContact?.invoke(contact)
    }

    override fun endContact(contact: Contact) {
        onEndContact?.invoke(contact)
    }

    override fun preSolve(contact: Contact, oldManifold: Manifold) {
        beforeSolve?.invoke(contact, oldManifold)
    }

    override fun postSolve(contact: Contact, impulse: ContactImpulse) {
        afterSolve?.invoke(contact, impulse)
    }
}
