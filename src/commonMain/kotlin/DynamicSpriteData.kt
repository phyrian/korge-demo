import org.jbox2d.callbacks.ContactImpulse
import org.jbox2d.collision.Manifold
import org.jbox2d.dynamics.Fixture
import org.jbox2d.dynamics.contacts.Contact

class DynamicSpriteData(callback: DynamicSpriteData.() -> Unit = {}) {
    var onBeginContact: ((fixture: Fixture?, other: Fixture?, contact: Contact) -> Unit)? = null
    var onEndContact: ((fixture: Fixture?, other: Fixture?, contact: Contact) -> Unit)? = null
    var beforeSolve: ((fixture: Fixture?, other: Fixture?, contact: Contact, oldManifold: Manifold) -> Unit)? = null
    var afterSolve: ((fixture: Fixture?, other: Fixture?, contact: Contact, impulse: ContactImpulse) -> Unit)? = null
    val extra: HashMap<String, Any?> = hashMapOf()

    init {
        this.apply(callback)
    }
}
