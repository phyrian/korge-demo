import com.soywiz.klock.milliseconds
import com.soywiz.korge.box2d.WorldView
import com.soywiz.korio.async.async
import com.soywiz.korio.async.delay
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

@Suppress("unused")
suspend fun WorldView.afterUnlock(callback: WorldView.() -> Unit) {
    async(coroutineContext) {
        while (world.isLocked) {
            delay(5.milliseconds)
        }
        callback()
    }.start()
}

fun WorldView.afterUnlock(context: CoroutineContext, callback: WorldView.() -> Unit) {
    async(context) {
        while (world.isLocked) {
            delay(5.milliseconds)
        }
        callback()
    }.start()
}
