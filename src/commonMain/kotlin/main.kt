import com.soywiz.korge.Korge
import com.soywiz.korgw.GameWindow
import com.soywiz.korio.async.launchImmediately
import com.soywiz.korte.Tag
import com.soywiz.korte.Template
import com.soywiz.korte.TemplateConfig

suspend fun main() = Korge(quality = GameWindow.Quality.PERFORMANCE) {
    launchImmediately {
    }

//    load(PongGame())
//    load(WhatAmIDrawing())
    load(RagDollTestGame())

//    text("").apply {
//        this.setHtml(resourcesVfs["demo.menu.html"].readString())
//    }
}
