import com.soywiz.korge.box2d.*
import com.soywiz.korge.view.SolidRect
import com.soywiz.korim.color.Colors.RED
import org.jbox2d.collision.shapes.CircleShape
import org.jbox2d.common.Vec2
import org.jbox2d.dynamics.joints.DistanceJoint
import org.jbox2d.dynamics.joints.DistanceJointDef

class RagDoll {

    companion object {
        fun WorldView.createRagDoll(x: Float, y: Float, gravityScale: Float = 1.0f) {
            val body = createBody {
                setPosition(x - 2f, y + 1.5f)
                this.gravityScale = gravityScale
            }.fixture {
                shape = BoxShape(5, 7)
            }.setView(SolidRect(5, 7, RED))

            val head = createBody {
                setPosition(x - 1.5f, y - 1.5f)
                this.gravityScale = gravityScale
            }.fixture {
                shape = CircleShape().apply { this.m_radius = 3.0f }
            }.setView(SolidRect(3, 3, RED))

            val leftArm = createBody {
                setPosition(x - 4f, y + 1.5f)
                this.gravityScale = gravityScale
            }.fixture {
                shape = BoxShape(2, 8)
            }.setView(SolidRect(2.0, 8.0, RED))

            val rightArm = createBody {
                setPosition(x + 4f, y + 1.5f)
                this.gravityScale = gravityScale
            }.fixture {
                shape = BoxShape(2, 8)
            }.setView(SolidRect(2.0, 8.0, RED))

            val leftLeg = createBody {
                setPosition(x - 2f, y + 8.5f)
                this.gravityScale = gravityScale
            }.fixture {
                shape = BoxShape(2, 8)
            }.setView(SolidRect(2.0, 8.0, RED))

            val rightLeg = createBody {
                setPosition(x + 2f, y + 8.5f)
                this.gravityScale = gravityScale
            }.fixture {
                shape = BoxShape(2, 8)
            }.setView(SolidRect(2.0, 8.0, RED))

            DistanceJoint(this.world.pool, DistanceJointDef().apply { this.initialize(body, head, Vec2(0f, 3f), Vec2(3f, 1.5f)) })
            DistanceJoint(this.world.pool, DistanceJointDef().apply { this.initialize(body, leftArm, Vec2(0f, 0f), Vec2(1f, 0f)) })
            DistanceJoint(this.world.pool, DistanceJointDef().apply { this.initialize(body, rightArm, Vec2(5f, 0f), Vec2(1f, 0f)) })
            DistanceJoint(this.world.pool, DistanceJointDef().apply { this.initialize(body, leftLeg, Vec2(1f, 7f), Vec2(1f, 0f)) })
            DistanceJoint(this.world.pool, DistanceJointDef().apply { this.initialize(body, rightLeg, Vec2(4f, 7f), Vec2(1f, 0f)) })
        }
    }

}
