import IContactListener.Companion.default
import com.soywiz.korev.Key
import com.soywiz.korge.box2d.*
import com.soywiz.korge.input.onKeyDown
import com.soywiz.korge.input.onKeyUp
import com.soywiz.korge.view.anchor
import com.soywiz.korge.view.position
import com.soywiz.korge.view.scale
import com.soywiz.korge.view.solidRect
import com.soywiz.korim.color.Colors
import com.soywiz.korim.color.RGBA
import org.jbox2d.collision.shapes.CircleShape
import org.jbox2d.common.MathUtils
import org.jbox2d.common.Vec2
import org.jbox2d.dynamics.Body
import org.jbox2d.dynamics.BodyType
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.random.Random

class PongGame : Level() {

    private val bat = "bat"
    private val batSpeed = 20.0

    private var isIdle = true

    private var ball: Body? = null

    override suspend fun setup() {
        val parent0 = parent!!
        val windowWidth = parent0.width
        val windowHeight = parent0.height

        val worldScale = 20f
        val worldWidth = (windowWidth / worldScale).toFloat()
        val worldHeight = (windowHeight / worldScale).toFloat()
        val worldCenterX = worldWidth / 2f
        val worldCenterY = worldHeight / 2f

        worldView {
            world.gravity = Vec2()
            world.setContactListener(default)

            position(0, windowHeight)
            scale(worldScale)

            val p1x = 1.5f
            val p2x = worldWidth - p1x
            val p1 = createBat(p1x, worldCenterY, Key.W, Key.S)
            val p2 = createBat(p2x, worldCenterY, Key.UP, Key.DOWN)

            ball = createBall(worldCenterX, worldCenterY, DynamicSpriteData {
                afterSolve = { fixture, other, _, _ ->
                    if ((other?.userData as? DynamicSpriteData)?.extra?.get("type") == bat) {
                        val batVelocity = other.getBody()?.linearVelocity
                        if (batVelocity != null) {
                            afterUnlock(EmptyCoroutineContext) {
                                fixture?.getBody()?.apply {
                                    linearVelocity = Vec2(linearVelocity.x, linearVelocity.y + (batVelocity.y / 2))
                                }
                            }
                        }
                    }
                }
            })

            createWall(worldCenterX, worldHeight - .1f, worldWidth, .1f, Colors.DARKOLIVEGREEN)
            createWall(worldCenterX, .1f, worldWidth, .1f, Colors.DARKOLIVEGREEN)

            fun goalData(playerName: String): DynamicSpriteData {
                return DynamicSpriteData {
                    onEndContact = { _, other, _ ->
                        if (other?.getBody() == ball) {
                            isIdle = true
                            ball!!.linearVelocity = Vec2()
                            p1.linearVelocity = Vec2()
                            p2.linearVelocity = Vec2()
                            println("$playerName wins!")
                            afterUnlock(EmptyCoroutineContext) {
                                ball!!.setTransform(Vec2(worldCenterX, worldCenterY), 0f)
                                p1.setTransform(Vec2(p1x, worldCenterY), 0f)
                                p2.setTransform(Vec2(p2x, worldCenterY), 0f)
                            }
                        }
                    }
                }
            }
            createTrigger(-.05f, worldCenterY, .1f, worldHeight, goalData("P2"))
            createTrigger(worldWidth + .05f, worldCenterY, .1f, worldHeight, goalData("P1"))
        }
    }

    override suspend fun start() {
        onKeyDown {
            if (it.key == Key.SPACE && isIdle) {
                ball!!.linearVelocity = Vec2(
                        (if (Random.nextBoolean()) 1f else -1f) * 20f,
                        (if (Random.nextBoolean()) 1f else -1f) * MathUtils.randomFloat(2.0f, 5.5f)
                )
                isIdle = false
            }
        }
    }

    private fun WorldView.createBall(x: Float, y: Float, ballData: DynamicSpriteData): Body {
        return createBody {
            type = BodyType.DYNAMIC
            setPosition(x, y)
        }.fixture {
            shape = CircleShape()
            shape!!.m_radius = .5f
            restitution = 1.01f
            friction = 0f
            density = .1f
            userData = ballData
        }.apply {
            setView(solidRect(1f, 1f, Colors.LIMEGREEN).anchor(.5f, .5f))
        }
    }

    private fun WorldView.createWall(x: Float, y: Float, width: Float, height: Float, color: RGBA): Body {
        return createBody {
            setPosition(x, y)
        }.fixture {
            shape = BoxShape(width, height)
        }.apply {
            setView(solidRect(width, height, color).anchor(.5, .5))
        }
    }

    private fun WorldView.createTrigger(x: Float, y: Float, width: Float, height: Float, fixtureData: DynamicSpriteData): Body {
        return createBody {
            setPosition(x, y)
        }.fixture {
            shape = BoxShape(width, height)
            isSensor = true
            userData = fixtureData
        }
    }

    private fun WorldView.createBat(x: Float, y: Float, upKey: Key, downKey: Key): Body {
        return createBody {
            type = BodyType.DYNAMIC
            fixedRotation = true
            speed = batSpeed
            setPosition(x, y)
        }.fixture {
            shape = BoxShape(1, 10)
            density = 10000f
            userData = DynamicSpriteData {
                extra["type"] = bat
            }
        }.apply {
            setView(solidRect(1f, 10f, Colors.LIMEGREEN).anchor(.5, .5))
            attachYAxisSmoothControl(upKey, downKey) { isIdle }
        }
    }

    private fun Body.attachYAxisSmoothControl(upKey: Key, downKey: Key, preventControlPredicate: () -> Boolean = { false }) {
        val controlKeys = arrayOf(upKey, downKey)
        var movingUp = false
        var movingDown = false
        onKeyDown {
            if (it.key in controlKeys && !preventControlPredicate()) {
                when (it.key) {
                    upKey -> movingUp = true
                    downKey -> movingDown = true
                    else -> {
                    }
                }
                val yVel =
                        if (movingUp && !movingDown) speed.toFloat()
                        else if (movingDown && !movingUp) -speed.toFloat()
                        else 0f
                linearVelocity = Vec2(linearVelocity.x, yVel)
            }
        }
        onKeyUp {
            if (it.key in controlKeys && !preventControlPredicate()) {
                when (it.key) {
                    upKey -> movingUp = false
                    downKey -> movingDown = false
                    else -> {
                    }
                }
                val yVel =
                        if (movingUp && !movingDown) speed.toFloat()
                        else if (movingDown && !movingUp) -speed.toFloat()
                        else 0f
                linearVelocity = Vec2(linearVelocity.x, yVel)
            }
        }
    }

}
