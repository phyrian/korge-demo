import com.soywiz.korev.Key
import com.soywiz.korge.html.Html
import com.soywiz.korge.input.*
import com.soywiz.korge.view.*
import com.soywiz.korim.color.Colors
import com.soywiz.korim.vector.Context2d
import com.soywiz.korio.util.endExclusive
import com.soywiz.korma.geom.*
import com.soywiz.korma.geom.shape.Shape2d
import com.soywiz.korma.geom.shape.bounds
import com.soywiz.korma.geom.vector.moveTo
import kotlin.math.abs
import kotlin.math.sqrt

private var eps = 10.0f

class WhatAmIDrawing : Level() {
    private val store: Map<PointArrayList, String> = mutableMapOf()

    private var isDown = false
    private var lastMoveA: MutableList<Point>? = null
    private var lastMoveB: MutableList<Point>? = null

    override suspend fun setup() {
        val game = this@WhatAmIDrawing
        val parent0 = parent!!
        val windowWidth = parent0.width
        val windowHeight = parent0.height

        val worldScale = 1f
        val worldWidth = (windowWidth / worldScale).toFloat()
        val worldHeight = (windowHeight / worldScale).toFloat()
        val worldCenterX = worldWidth / 2f
        val worldCenterY = worldHeight / 2f

        val eText = game.text(eps.toString(), textSize = 50.0, color = Colors.WHITE) {
            position(100, worldHeight - 75)
            autoSize = false
            filtering = false
            setTextBounds(Rectangle(0, 0, windowWidth - 175, 50.0))
            format.align = Html.Alignment.CENTER
            onClick { println(eps.toString()) }
        }
        val decreaseEpsilon = {
            if (eps > 0.0f) {
                eps -= 1.0f
                eText.text = eps.toString()
            }
        }
        val increaseEpsilon = {
            if (eps < 100.0f) {
                eps += 1.0f
                eText.text = eps.toString()
            }
        }

        val minus = game.buttonBase(50.0, 50.0) {
            position(50, worldHeight - 75)
            onClick { decreaseEpsilon() }
        }.text("-", textSize = 50.0, color = Colors.RED) {
            filtering = false
        }

        val plus = game.buttonBase(50.0, 50.0) {
            position(windowWidth - 75, worldHeight - 75)
            onClick { increaseEpsilon() }
        }.text("+", textSize = 50.0, color = Colors.RED) {
            filtering = false
        }

        onKeyDown {
            when {
                it.key == Key.KP_SUBTRACT -> decreaseEpsilon()
                it.key == Key.KP_ADD -> increaseEpsilon()
            }
        }

        var shapeA: Shape2d.Polyline? = null
        var graphA: Graphics? = null
        solidRect(worldWidth / 2, worldHeight - 100, Colors.ALICEBLUE) {
            position(0, 0)
            scale(worldScale)
            this.onDown {
                if (!isDown) {
                    lastMoveA = mutableListOf()
                    graphA?.removeFromParent()
                }
                isDown = true
            }

            this.onMove {
                if (isDown) {
                    lastMoveA!! += Point(it.currentPos)
                }
            }

            this.onUp {
                isDown = false
                if (lastMoveA != null) {
                    shapeA = Shape2d.Polyline(lastMoveA!!.toPointArrayList())
                    graphA = shapeA!!.draw(game.graphics(), Context2d.Color(Colors.BLACK), Context2d.StrokeInfo(3.0))
                }
            }

        }

        var shapeB: Shape2d.Polyline? = null
        var graphB: Graphics? = null
        solidRect(worldWidth / 2, worldHeight - 100, Colors.LIGHTGOLDENRODYELLOW) {
            position(worldCenterX, 0)
            scale(worldScale)
            this.onDown {
                if (!isDown) {
                    lastMoveB = mutableListOf()
                    graphB?.removeFromParent()
                }
                isDown = true
            }

            this.onMove {
                if (isDown) {
                    lastMoveB!! += Point(it.currentPos.x + worldCenterX, it.currentPos.y)
                }
            }

            this.onUp {
                isDown = false
                if (lastMoveB != null) {
                    shapeB = Shape2d.Polyline(lastMoveB!!.toPointArrayList())
                    graphB = shapeB!!.draw(game.graphics(), Context2d.Color(Colors.BLACK), Context2d.StrokeInfo(3.0))
                }
            }

        }

        this.onKeyUp {
            if (it.key == Key.SPACE
                    && shapeA != null && graphA != null
                    && shapeB != null && graphB != null) {
                graphA!!.removeFromParent()
                graphB!!.removeFromParent()

                var listA = DouglasPeucker(lastMoveA!!.rebase())
                var listB = DouglasPeucker(lastMoveB!!.rebase())

                val wa = shapeA!!.bounds.width
                val wb = shapeB!!.bounds.width
                val dx = abs(wa - wb)
                val pdx = (dx / ((wa + wb) / 2))
                if (wa < wb) {
                    listA = listA.scale(1 + pdx)
                } else {
                    listB = listB.scale(1 + pdx)
                }

                graphA = Shape2d.Polyline(listA.toPointArrayList())
                        .draw(game.graphics(), Context2d.Color(Colors.BLACK), Context2d.StrokeInfo(3.0))
                graphB = Shape2d.Polyline(listB.moveBy(worldCenterX.toDouble(), 0.0).toPointArrayList())
                        .draw(game.graphics(), Context2d.Color(Colors.BLACK), Context2d.StrokeInfo(3.0))
            }
        }
    }

    override suspend fun start() {
    }
}

private fun Shape2d.Polyline.draw(graphics: Graphics, color: Context2d.Color, info: Context2d.StrokeInfo): Graphics {
    val poly = this
    return graphics.apply {
        beginStroke(color, info)
        poly.points.toPoints().forEachIndexed { i, p ->
            if (i == 0) this.moveTo(p) else this.lineTo(p.x, p.y)
        }
        endStroke()
    }
}

private fun Iterable<Point>.toPointArrayList(): IPointArrayList {
    val r = PointArrayList()
    forEach { r.add(it) }
    return r
}

private fun Iterable<Point>.rebase(base: IPoint = Point.Zero): List<Point> {
    val dx = minBy { it.x }!!.x
    val dy = minBy { it.y }!!.y
    val d = Point(dx, dy)
    return map { Point(it - d + base) }
}

private fun Iterable<Point>.moveBy(x: Double, y: Double): List<Point> {
    return map { Point(it.x + x, it.y + y) }
}

private fun Iterable<Point>.scale(x: Double, y: Double = x): List<Point> {
    return map { Point(it.x * x, it.y * y) }
}

private operator fun <T> List<T>.get(range: IntRange): List<T> {
    return this.subList(range.first, range.endExclusive)
}

private fun perpendicularDistance(point: Point, linePoint1: Point, linePoint2: Point): Double {
    val x0 = point.x
    val y0 = point.y

    val x1 = linePoint1.x
    val y1 = linePoint1.y

    val x2 = linePoint2.x
    val y2 = linePoint2.y

    return abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1) / sqrt(((y2 - y1) * (y2 - y1)) + ((x2 - x1) * (x2 - x1)))
}

private fun DouglasPeucker(pointList: List<Point>, epsilon: Float = eps): List<Point> {
    if (pointList.size <= 2) {
        return pointList
    }

    // Find the point with the maximum distance
    var dmax = 0.0
    var index = 0
    val end = pointList.lastIndex
    for (i in 0..end) {
        val d = perpendicularDistance(pointList[i], pointList[0], pointList[end])
        if (d > dmax) {
            index = i
            dmax = d
        }
    }

    // If max distance is greater than epsilon, recursively simplify
    return if (dmax > epsilon) {
        // Recursive call
        val recResults1 = DouglasPeucker(pointList[0..index], epsilon)
        val recResults2 = DouglasPeucker(pointList[index..end], epsilon)

        // Build the result list
        recResults1[0 until recResults1.lastIndex] + recResults2[0..recResults2.lastIndex]
    } else {
        listOf(pointList.first(), pointList.last())
    }
}
