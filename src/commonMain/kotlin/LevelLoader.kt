import com.soywiz.klock.seconds
import com.soywiz.korge.tween.hide
import com.soywiz.korge.tween.show
import com.soywiz.korge.view.*
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.format.readBitmap
import com.soywiz.korio.async.async
import com.soywiz.korio.file.std.resourcesVfs
import com.soywiz.korma.interpolation.Easing
import kotlin.coroutines.coroutineContext

abstract class Level : Container() {

    open suspend fun levelLoader(stage: Stage): LevelLoader {
        return defaultLoader(stage)
    }

    open suspend fun setup() {
        //noop
    }

    open suspend fun start() {
        //noop
    }

}

class LevelLoader(parent: Container) : Container() {

    private val _parent = parent

    private var beforeLoad: suspend LevelLoader.(level: Level) -> Unit = {}
    private var whileLoading: suspend LevelLoader.(level: Level) -> Unit = {}
    private var afterLoad: suspend LevelLoader.(level: Level) -> Unit = {}

    fun beforeLoad(callback: suspend LevelLoader.(level: Level) -> Unit): LevelLoader {
        beforeLoad = callback
        return this
    }

    fun whileLoading(callback: suspend LevelLoader.(level: Level) -> Unit): LevelLoader {
        whileLoading = callback
        return this
    }

    fun afterLoad(callback: suspend LevelLoader.(level: Level) -> Unit): LevelLoader {
        afterLoad = callback
        return this
    }

    suspend fun load(level: Level, clearStage: Boolean = true): View {
        if (clearStage) {
            _parent.removeAllComponents()
            _parent.removeChildren()
        }

        // Transparent containment, loader above level
        _parent += level
        _parent += this

        beforeLoad(level)

        var loaded = false
        async(coroutineContext) {
            level.setup()
            loaded = true
        }.start()

        while (!loaded) {
            whileLoading(level)
        }

        afterLoad(level)

        // Remove loader
        _parent -= this

        level.show(1.seconds)
        level.start()
        return level
    }
}

suspend fun Stage.load(level: Level) {
    level.levelLoader(this).load(level)
}

private var bitmap: Bitmap? = null
suspend fun defaultLoader(stage: Stage): LevelLoader {
    if (bitmap == null) {
        bitmap = resourcesVfs["korge.png"].readBitmap()
    }

    var image: Image? = null
    return LevelLoader(stage)
            .beforeLoad {
                it.alpha = 0.0
                image = image(bitmap!!) {
                    name = "loader"
                    anchor(.5, .5)
                    position(stage.width / 2, stage.height / 2)
                    alpha = .0
                }
            }
            .whileLoading {
                image!!.show(1.seconds, Easing.EASE_IN_QUAD)
                image!!.hide(1.seconds, Easing.EASE_OUT_QUAD)
            }
            .afterLoad {
                it.show(1.seconds)
            }
}
