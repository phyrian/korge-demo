import com.soywiz.korge.view.*
import com.soywiz.korim.bitmap.Bitmaps

inline fun Container.buttonBase(
        width: Number, height: Number, callback: @ViewsDslMarker ButtonBase.() -> Unit = {}
) = ButtonBase(width.toDouble(), height.toDouble()).addTo(this).apply(callback)

class ButtonBase(width: Double, height: Double) : RectBase() {
    companion object {
        operator fun invoke(width: Number, height: Number) =
                ButtonBase(width.toDouble(), height.toDouble())
    }

    override var width: Double = width
        set(v) = run { field = v }.also { dirtyVertices = true }
    override var height: Double = height
        set(v) = run { field = v }.also { dirtyVertices = true }

    init {
        this.baseBitmap = Bitmaps.transparent
    }

    override fun createInstance(): View = ButtonBase(width, height)
}
