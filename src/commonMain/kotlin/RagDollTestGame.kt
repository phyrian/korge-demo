import IContactListener.Companion.default
import RagDoll.Companion.createRagDoll
import com.soywiz.korev.Key
import com.soywiz.korge.box2d.*
import com.soywiz.korge.input.onClick
import com.soywiz.korge.input.onKeyDown
import com.soywiz.korge.input.onKeyUp
import com.soywiz.korge.view.anchor
import com.soywiz.korge.view.position
import com.soywiz.korge.view.scale
import com.soywiz.korge.view.solidRect
import com.soywiz.korim.color.Colors
import com.soywiz.korim.color.RGBA
import org.jbox2d.collision.shapes.CircleShape
import org.jbox2d.common.MathUtils
import org.jbox2d.common.Vec2
import org.jbox2d.dynamics.Body
import org.jbox2d.dynamics.BodyType
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.random.Random

class RagDollTestGame : Level() {

    private lateinit var worldView: WorldView
    private var ragDoll: RagDoll? = null

    override suspend fun setup() {
        val parent0 = parent!!
        val windowWidth = parent0.width
        val windowHeight = parent0.height

        val worldScale = 20f
        val worldWidth = (windowWidth / worldScale).toFloat()
        val worldHeight = (windowHeight / worldScale).toFloat()
        val worldCenterX = worldWidth / 2f
        val worldCenterY = worldHeight / 2f

        worldView = worldView {
            world.gravity = Vec2()
            world.setContactListener(default)

            position(0, windowHeight)
            scale(worldScale)
            createRagDoll(worldCenterX, worldCenterY, 1.0f)
            createWall(worldCenterX, worldHeight - .1f, worldWidth, .1f, Colors.DARKOLIVEGREEN)
            createWall(worldCenterX, .1f, worldWidth, .1f, Colors.DARKOLIVEGREEN)
        }
    }

    override suspend fun start() {
        onClick {
//            worldView.createRagDoll(it.currentPos.x.toFloat(), it.currentPos.y.toFloat(), 1.0f)
        }
    }

    private fun WorldView.createWall(x: Float, y: Float, width: Float, height: Float, color: RGBA): Body {
        return createBody {
            setPosition(x, y)
        }.fixture {
            shape = BoxShape(width, height)
        }.setView(solidRect(width, height, color).anchor(.5, .5))
    }

}
