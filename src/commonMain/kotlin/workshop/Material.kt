package workshop

/**
 * Created by GPederi.
 */
enum class Material {
    WOOD, NATURAL_RUBBER, RUBBER, TEXTILE, LATEX, LEATHER, COPPER, IRON, COAL, SUGAR, SALTPETER, STEEL,
    CRYSTAL_POWDER, CRYSTAL_ORE
}
